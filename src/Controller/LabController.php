<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LabController extends AbstractController
{
    #[Route('/lab', name: 'app_lab')]
    public function index(): Response
    {
        return $this->render('lab/lab.html.twig', [
            'controller_name' => 'LabController',
        ]);
    }
    #[Route('/form', name: 'app_form')]
    public function form(): Response
    {
        return $this->render('lab/form.html.twig', [
            'controller_name' => 'LabController',
        ]);
    }
}
